#include <cstdio>
#include <vector>
#include <algorithm>
#include <iostream>
#include <tuple>

using namespace std;

//Es una tupla ( costo, pozo1, pozo2 )
typedef tuple<int, int, int> Tuberia;

//Encuentra el padre en la estructura "parent" para el elemento x.
int findset(int x, vector<tuple<unsigned int, unsigned int>> &parent);

//Viendo a "tuberias" como un grafo ponderado, Kruskal genera el bosque recubridor minimo y lo guarda en "MST".
void kruskal( vector<Tuberia> &tuberias,  vector<tuple<unsigned int, unsigned int>> &parent, vector<Tuberia> &MST, int &costoTotal, int &cantPozos );

int main(){

    int costoTotal = 0;
    int cantRefinerias = 0;

    int cantPozos = 0;
    int cantPosiblesTuberias = 0;
    int costoRefineria = 0;

    int pozo1 = 0;
    int pozo2 = 0;
    int costoTuberia = 0;

    vector<Tuberia> tuberias;
    vector<Tuberia> MST; //Minimum spanning tree ( Arbol recubridor minimo )

    cin >> cantPozos >> cantPosiblesTuberias >> costoRefineria;

    //Cada elemento i de parent es una tupla, donde su primera componente indica el padre de i (el indice j del pozo que es la raiz del arbol al cual pertenece)
    //y su segunda componente es la altura del arbol generado hacia "abajo" tomando a i como raiz (contandola a partir de 0).
    vector<tuple<unsigned int, unsigned int>> parent(cantPozos);
    for (int i = 0 ; i < cantPozos ; i++ ) parent[i] = make_tuple( i , 0 );

    //Registro todas las tuberias posibles, pero filtrando las que posean un costo mayor o igual al costo de una refineria.
    //Se almacenan en tuplas, tomando como primera componente al costo, esto es necesario para luego hacer "sorting".
    for( int i = 1 ; i <= cantPosiblesTuberias ; i++){

        cin >> pozo1 >> pozo2 >> costoTuberia;

        if( pozo1 != pozo2 && pozo1 > 0 && pozo2 > 0 && costoTuberia < costoRefineria ){

                Tuberia tempTuberia = make_tuple(costoTuberia, pozo1, pozo2);

                tuberias.push_back(tempTuberia);
            }
    }

    //Creo un heap y hago "HeapSort", para luego poder acceder en en tiempo constante al siguiente elemento de menor costo.
    make_heap( tuberias.begin(), tuberias.end() );
    sort_heap( tuberias.begin(), tuberias.end() );

    kruskal(tuberias, parent, MST, costoTotal, cantPozos);

    //La cantidad de refinerias necesarias es la cantidad de arboles distintos que se infieren del MST
    //Si w es la cantidad de arboles en MST, "#pozos" = "#pozosEnArbol_1" + ... +"#pozosEnArbol_w" y "#MST" = "#aristasDelArbol_1" + ... + "#aristasDelArbol_w"
    //Y si n_w es la cantidad de vertices del arbol w (es "#pozosEnArbol_w") y cada arbol cumple que tiene m_w aristas con m_w = n_w - 1
    //Entonces "#refinerias" = "#pozos" - "#MST" = n_1 - m_1 + ... n_w - m_w = n_1 - n_1 + 1 + ... n_w - n_w + 1 = Sumatoria de 1 a w de unos = w = la cantidad de arboles en MST.

    cantRefinerias = cantPozos - MST.size();
    costoTotal += costoRefineria*cantRefinerias;

    //Imprimo la salida en pantalla:

    // costo total (tuberias + refinerias), la minima cantidad de refinerias necesarias para nuestra configuracion, y la cantidad de tuberias de la solucion.
    cout << costoTotal << " " << cantRefinerias << " " << MST.size() << endl;

    //Los pozos en donde se han elegido poner una refineria. Corresponden a las raices de los arboles generados tras hacer kruskal.
    for(int i = 0 ; i < cantPozos ; i++ ){

        if ((int)get<0>(parent[i]) == i) cout <<  i + 1 << endl;
    }

    //Las tuberias elegidas, identificadas por los pozos que conectan entre si.
    for(int i = 0 ; i < (int)MST.size() ; i++ ){

        cout << get<1>(MST[i]) << " " << get<2>(MST[i]) << endl;
    }

    return 0;
}

//Dado un nodo x, revisa a su padre directo, y a su vez al padre directo de este, y asi sucesivamente hasta encontrar a alguien que sea padre de si mismo, este sera la raiz del arbol al que pertenece.
int findset(int x, vector<tuple<unsigned int, unsigned int>> &parent){

    if(x != (int)get<0>(parent[x]))
        get<0>(parent[x]) = findset(get<0>(parent[x]), parent);
    return get<0>(parent[x]);
}

void kruskal( vector<Tuberia> &tuberias, vector<tuple<unsigned int, unsigned int>> &parent, vector<Tuberia> &MST, int &costoTotal, int &cantPozos ){

    int i, p1, p2;

    for(i = 0 ; i < (int)tuberias.size(); i++)
    {
        //Guardo en p1 y p2 los padres en la estructura "parent" de los pozos que conecta la tuberia i (aqui con padres nos referimos a la raiz del arbol al que pertenece cada uno).

        p1 = findset(get<1>(tuberias[i]) - 1, parent);

        p2 = findset(get<2>(tuberias[i]) - 1, parent);

        //Si los padres son distintos, significa que puedo conectar el pozo1 con el pozo2 sin hacer un ciclo,
        //si no ya estan conectados por algun otro eje al mismo arbol, y este eje es posee menor o igual costo, pues lo habiamos sacado antes en el arreglo ordenado.
        if(p1 != p2)
        {

            //Agrego el eje al actual arbol recubridor minimo y le incremento al costoTotal el costo del nuevo vertice.
            MST.push_back(tuberias[i]);
            costoTotal += get<0>(tuberias[i]);

            //Si el arbol generado por el padre del pozo1 es mayor o igual al generado por el padre del pozo2 (en lo que a altura se refiere), conecto a este ultimo como hijo directo del primero.
            //En caso de que ocurra lo contrario, el padre del pozo1 es el que se conecta como hijo directo del padre del pozo2.
            if ( get<1>(parent[p1]) >= get<1>(parent[p2]) ){

                //Si ambos tenian la misma altura, aumento la altura del padre del pozo1 en uno.
                if (get<1>(parent[p2]) == get<1>(parent[p2]))
                    parent[p2] = make_tuple( p1, get<1>(parent[p1]));
                else
                    parent[p1] = make_tuple( p1, get<1>(parent[p1]) + 1);

                parent[p2] = make_tuple( p1, get<1>(parent[p2]));


            } else {

                parent[p1] = make_tuple( p2, get<1>(parent[p1]));
                parent[p2] = make_tuple( p2, get<1>(parent[p2]));

            }
        }
    }
}
