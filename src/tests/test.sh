#!/bin/bash
# GO TO THE BOTOM TO SET TESTS

#Clear output file

#Create basic input files for generator
echo "1 1 1 0 0 0 0 0 0 0" > exercice1 #Dakkar
echo "2 1 1 0 0 0 0 0 0 0" > exercice2 #Zombieland2
echo "3 1 1 0 0 0 0 0 0 0" > exercice3 #Petroleo

#Clear the txt of some test
#Params
	#$1 ejecutable
	#$2 exercice number
function CLEAR {
	#Clear the output files
	echo "N,T (ns),TDN" > "output${1}${2}.csv"
	> "output${1}${2}.log"
}

#Get time in nanoseconds
#Params
	#None
function TIME {
	date +%s%N
}

#Run the program with the desired params
#Params
	#$1 ejecutable
	#$2 ejercice number
	#$3 size of test
	#$4 special param
	#$5 special param2
function RUN {

	wait
	otime=$(TIME) #Time before calling ejecutable
	./$1 < generator.txt >> "output${1}${2}.log"
	ctime=$(TIME) #Time after calling ejecutable
	dtime=$((ctime - otime)) #Calculate delta time
	printf ",${dtime}" >> "output${1}${2}.csv"

	if [ "${1}" == "dakkar" ]
	then
		printf "," >> "output${1}${2}.csv"
		float=`echo "${dtime} / ( ${3} * ${4} * ${5} )" | bc -l`
		echo ${float%.*}  >> "output${1}${2}.csv"
	else
		echo ",0" >> "output${1}${2}.csv"
	fi

	echo "Time: ${dtime}ns"
}

#Make a input with $2 size and test output $1 ammount of times
#Params
	#$1 ammount of tests
	#$2 size of tests
	#$3 program ejecutable
	#$4 exercice number
	#$5 special param
	#$6 special param2
function TEST {
	echo "Starting $1 random test/s with ${3} (Exercice ${4}), size ${2}"

	for i in `seq 1 ${1}`
	do
		echo $i

		t="$(TIME)"
		echo "${4} $((i + t % 1000)) ${2} ${5} ${6}" > exercice$4
		./generator < exercice$4 > generator.txt 
		./generator < exercice$4 >> "output${3}${4}.log"
		printf "${2}" >> "output${3}${4}.csv"
		RUN $3 $4 $2 $5 $6

		echo " " >> "output${3}${4}.log"
	done
}

#Run a lots of test (made with the generator) of and ejecutable
	#$1 ammount of tests per size
	#$2 program ejecutable
	#$3 exercice number
function REPEATTESTS1 {
	for i in ${sizes[*]}
	do
		TEST $1 $i $2 $3 $i $i
	done
}

# sizes=(100 200 300 400 500 600 700 800 900 1000 1100 1200 1300 1400 1500 1600 1700 1800 1900 2000 2250 2500 2750 3000 3250 3500 3750 4000 4250 4500 4750 5000 5500 6000 6500 7000 7500 8000 8500 9000 9500 10000 11000 12000 13000 14000 15000 16000 17000 18000 19000 20000 22500 25000 27500 30000 32500 35000 37500 40000 42500 45000 47500 50000)

#Run tests
#Dakkar E1
CLEAR dakkar1
sizes=(10 20 30 40 50 60 70 80 90 100 200 300 400 500 600 700 800 900 1000 1250 1500 1750 2000 2500 3000 3500 4000 5000)
REPEATTESTS1 10 dakkar 1
> outputdakkar1.log

#ZombielandII E2
# CLEAR zombie
# sizes=(100 200 300 400 500 600 700 800 900 1000 1100 1200 1300 1400 1500 1600 1700 1800 1900 2000 2250 2500 2750 3000 3250 3500 3750 4000 4250 4500 4750 5000 5500 6000 6500 7000 7500 8000 8500 9000 9500 10000 11000 12000 13000 14000 15000 16000 17000 18000 19000 20000)
# REPEATTESTS 10 zombie 2

#Petroleo E3
# CLEAR petroleo
# sizes=(100 200 300 400 500 600 700 800 900 1000 1100 1200 1300 1400 1500 1600 1700 1800 1900 2000 2250 2500 2750 3000 3250 3500 3750 4000 4250 4500 4750 5000 5500 6000 6500 7000 7500 8000 8500 9000 9500 10000 11000 12000 13000 14000 15000 16000 17000 18000 19000 20000)
# REPEATTESTS 10 petroleo 3