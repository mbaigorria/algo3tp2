#include <iostream>
#include <vector>
#include <queue>
#include <algorithm>

using namespace std;

struct Node {  
    int soldiers;
    int hAddr; // hAddr of parent
    int vAddr; // vAddr of parent

    Node(): soldiers(-1), hAddr(-1), vAddr(-1) {}
};

struct Cords {
	int hAddr;
	int vAddr;

	Cords(int hAddr, int vAddr) : hAddr(hAddr), vAddr(vAddr) {}
};

// function prototypes
void printMap(vector<vector<int>> &hStreets, vector<vector<int>> &vStreets,
	int nHorizontal, int nVertical);
void dijkstraOnDrugs(int nHorizontal, int nVertical, int initHorizontal, int initVertical,
	vector<vector<Node>> &map, vector<vector<int>> &hStreets, vector<vector<int>> &vStreets);
void updatePath(Node &destNode, int d_hAddr, int d_vAddr,
	int hAddr, int vAddr, int s, int z, queue<Cords> &nodeQueue);
void getPath(int initHorizontal, int initVertical, vector<vector<Node>> &map);
int  getSoldiersLeft(int s, int z);

int main() {

	int nHorizontal = 0;
	int nVertical = 0;
	int nSoldiers;

	int initHorizontal;
	int initVertical;

	int bunkerHorizontal;
	int bunkerVertical;

	cin >> nHorizontal >> nVertical >> nSoldiers;
	cin >> initHorizontal >> initVertical;
	cin >> bunkerHorizontal >> bunkerVertical;

	vector<vector<Node>> map(nHorizontal, vector<Node>(nVertical));            // store node data.
	vector<vector<int>>  hStreets(nHorizontal, vector<int>(nVertical -1, 0));  // store zombies in h-streets
	vector<vector<int>>  vStreets(nHorizontal - 1, vector<int>(nVertical, 0)); // store zombies in v-streets

	// addresses now start at 0.
	initHorizontal--;
	initVertical--;
	bunkerHorizontal--;
	bunkerVertical--;

	int tempZombies = 0;

	// load cost vectors
	for (int i = 0; i < nHorizontal; i++)
	{
		for (int j = 0; j < nVertical - 1;  j++)
		{
			cin >> tempZombies;
			hStreets[i][j] = tempZombies;
		}
		if (i != nHorizontal - 1) {
			for (int j = 0; j < nVertical;  j++)
			{
				cin >> tempZombies;
				vStreets[i][j] = tempZombies;
			}
		}
	}

	map[initHorizontal][initVertical].soldiers = nSoldiers; // set soldiers on origin

	// printMap(hStreets, vStreets, nHorizontal, nVertical);
	dijkstraOnDrugs(nHorizontal, nVertical, initHorizontal, initVertical,
		map, hStreets, vStreets);

	if (map[bunkerHorizontal][bunkerVertical].hAddr != -1) { // path exists
		cout << map[initHorizontal][initVertical].soldiers << endl;
		getPath(bunkerHorizontal, bunkerVertical, map);
	} else if (initHorizontal == bunkerHorizontal && // edge case, start in the bunker.
			   initVertical   == bunkerVertical) {
		cout << nSoldiers;
	} else cout << 0 << endl;

}

void getPath(int initHorizontal, int initVertical, vector<vector<Node>> &map)
{
	if (map[initHorizontal][initVertical].hAddr == -1) { // root! 
		cout << initHorizontal + 1 << " " << initVertical + 1 << endl;
	} else {
		getPath(map[initHorizontal][initVertical].hAddr, map[initHorizontal][initVertical].vAddr, map);
		cout << initHorizontal + 1 << " " << initVertical + 1 << endl;
	}
}

void dijkstraOnDrugs(int nHorizontal, int nVertical, int initHorizontal, int initVertical,
	vector<vector<Node>> &map, vector<vector<int>> &hStreets, vector<vector<int>> &vStreets) {

	// create FIFO queue.
	queue<Cords> nodeQueue;

	// push initial node
	Cords root(initHorizontal, initVertical);
	nodeQueue.push(root);

	while (!nodeQueue.empty())
	{
		Cords& tmp = nodeQueue.front();
		Node& originNode = map[tmp.hAddr][tmp.vAddr];

		// check next 4 possible vertices
		if (tmp.hAddr - 1 >= 0) { // can go up!
			// cout << "up!" << endl;
			Node& destNode = map[tmp.hAddr-1][tmp.vAddr]; 
			int zombies = vStreets[tmp.hAddr-1][tmp.vAddr];
			int aux = tmp.hAddr - 1;
			updatePath(destNode, aux, tmp.vAddr, tmp.hAddr,
				tmp.vAddr, originNode.soldiers, zombies, nodeQueue);
		}
		if (tmp.hAddr + 1 < (int) map.size()) { // can go down!
			Node& destNode1 = map[tmp.hAddr+1][tmp.vAddr];
			int zombies = vStreets[tmp.hAddr][tmp.vAddr];
			int aux1 = tmp.hAddr + 1;
			updatePath(destNode1, aux1, tmp.vAddr, tmp.hAddr,
				tmp.vAddr, originNode.soldiers, zombies, nodeQueue);
		}
		if (tmp.vAddr + 1 < (int) map[0].size()) { // can go right!
			Node& destNode3 = map[tmp.hAddr][tmp.vAddr + 1]; 
			int zombies = hStreets[tmp.hAddr][tmp.vAddr];
			int aux2 = tmp.vAddr + 1;
			updatePath(destNode3, tmp.hAddr, aux2, tmp.hAddr,
				tmp.vAddr, originNode.soldiers, zombies, nodeQueue);
		}
		if (tmp.vAddr - 1 >= 0) { // can go left!
			Node& destNode2 = map[tmp.hAddr][tmp.vAddr-1]; 
			int zombies = hStreets[tmp.hAddr][tmp.vAddr-1];
			int aux3 = tmp.vAddr - 1;
			updatePath(destNode2, tmp.hAddr, aux3, tmp.hAddr,
				tmp.vAddr, originNode.soldiers, zombies, nodeQueue);
		}

		nodeQueue.pop();
	}
}

void updatePath(Node &destNode, int d_hAddr, int d_vAddr,
	int hAddr, int vAddr, int s, int z, queue<Cords> &nodeQueue)
{
	int soldiersLeft = getSoldiersLeft(s, z);
	if (soldiersLeft > 0 && soldiersLeft > destNode.soldiers) {
		destNode.soldiers = soldiersLeft;
		destNode.hAddr    = hAddr;
		destNode.vAddr    = vAddr;
		Cords addCords(d_hAddr, d_vAddr);
		nodeQueue.push(addCords); // calls default copy constructor
	}
}

int getSoldiersLeft(int s, int z)
{
	if (s >= z) return s;
				return max(2*s - z, 0);
}

void printMap(vector<vector<int>> &hStreets, vector<vector<int>> &vStreets,
	int nHorizontal, int nVertical)
{
	for (int i = 0; i < nHorizontal; i++)
	{
		for (int j = 0; j < nVertical - 1;  j++)
		{
			printf("\t%u\t", hStreets[i][j]);
		}
		printf("\n");
		if (i != nHorizontal - 1) {
			for (int j = 0; j < nVertical;  j++)
			{
				printf("%u\t\t", vStreets[i][j]);
			}
			printf("\n");
		}
	}
}