#include <iostream>
#include <deque>
#include <tuple>

using namespace std;

typedef deque<deque<deque<int>>> memoria;

int costoPDinamica(deque<int>& cBMX, deque<int>& cMoto, deque<int>& cBuggy,
                    int act, int eTotal, int uBuggy, int uMoto,
                    memoria& mem);

int costoRecursivo(deque<int>& cBMX, deque<int>& cMoto, deque<int>& cBuggy,
                    int act, int eTotal, int uBuggy, int uMoto);

int main() {
    int cantEtapas;
    int usosMoto;
    int usosBuggy;
    
    cin >> cantEtapas >> usosMoto >> usosBuggy;
    deque<int> costoBMX(cantEtapas);
    deque<int> costoMoto(cantEtapas);
    deque<int> costoBuggy(cantEtapas);
    
    for(int i = 0; i < cantEtapas; i++) {
        cin >> costoBMX[i];
        cin >> costoMoto[i];
        cin >> costoBuggy[i];
    }
    
    memoria costos(cantEtapas);
    memoria etapas(cantEtapas);
    int tBMX;
    int tMoto;
    int tBuggy;
    //int cTemp;
    
    for(int i = 0; i < cantEtapas; i++) {
        costos[i].resize(usosMoto + 1);
        etapas[i].resize(usosMoto + 1);
        for(int j = 0; j <= usosMoto; j++) {
            costos[i][j].resize(usosBuggy + 1);
            etapas[i][j].resize(usosBuggy + 1);
            for(int k = 0; k <= usosBuggy; k++) {
                costos[i][j][k] = -1;
                etapas[i][j][k] = -1;
            }
        }
    }
    
    // Camino y costo Iterativo con Programacion Dinamica

    for(int i = cantEtapas-1; i >= 0; i--) {
        for(int j = 0; j <= usosMoto; j++) {
            for(int k = 0; k <= usosBuggy; k++) {
                if(i == cantEtapas - 1) {
                    if(j == 0 && k == 0) {
                        costos[i][j][k] = costoBMX[i];
                        etapas[i][j][k] = 1;
                    } else if(j == 0) {
                        if(costoBMX[i] <= costoBuggy[i]) {
                            costos[i][j][k] = costoBMX[i];
                            etapas[i][j][k] = 1;
                        } else {
                            costos[i][j][k] = costoBuggy[i];
                            etapas[i][j][k] = 3;
                        }
                    } else if(k == 0) {
                        if(costoBMX[i] <= costoMoto[i]) {
                            costos[i][j][k] = costoBMX[i];
                            etapas[i][j][k] = 1;
                        } else {
                            costos[i][j][k] = costoMoto[i];
                            etapas[i][j][k] = 2;
                        }
                    } else {
                        if(costoBMX[i] <= costoMoto[i] && costoBMX[i] <= costoBuggy[i]) {
                            costos[i][j][k] = costoBMX[i];
                            etapas[i][j][k] = 1;
                        } else if(costoMoto[i] <= costoBuggy[i]){
                            costos[i][j][k] = costoMoto[i];
                            etapas[i][j][k] = 2;
                        } else {
                            costos[i][j][k] = costoBuggy[i];
                            etapas[i][j][k] = 3;                            
                        }
                    }
                } else {
                    tBMX = costoBMX[i] + costos[i+1][j][k];
                    if(j == 0 && k == 0) {
                        costos[i][j][k] = tBMX;
                        etapas[i][j][k] = 1;
                    } else if(j == 0) {
                        tBuggy = costoBuggy[i] + costos[i+1][j][k-1];
                        if(tBMX <= tBuggy) {
                            costos[i][j][k] = tBMX;
                            etapas[i][j][k] = 1;
                        } else {
                            costos[i][j][k] = tBuggy;
                            etapas[i][j][k] = 3;
                        }
                    } else if(k == 0) {
                        tMoto = costoMoto[i] + costos[i+1][j-1][k];
                        if(tBMX <= tMoto) {
                            costos[i][j][k] = tBMX;
                            etapas[i][j][k] = 1;
                        } else {
                            costos[i][j][k] = tMoto;
                            etapas[i][j][k] = 2;
                        }
                    } else {
                        tMoto = costoMoto[i] + costos[i+1][j-1][k];
                        tBuggy = costoBuggy[i] + costos[i+1][j][k-1];
                        if(tBMX <= tMoto && tBMX <= tBuggy) {
                            costos[i][j][k] = tBMX;
                            etapas[i][j][k] = 1;
                        } else if(tMoto <= tBuggy){
                            costos[i][j][k] = tMoto;
                            etapas[i][j][k] = 2;
                        } else {
                            costos[i][j][k] = tBuggy;
                            etapas[i][j][k] = 3;                            
                        }
                    }
                }
            }
        }
    }

    //cTemp = costoPDinamica(costoBMX, costoMoto, costoBuggy, 0, cantEtapas, usosBuggy, usosMoto, costos);
    //cTemp = costoRecursivo(costoBMX, costoMoto, costoBuggy, 0, cantEtapas, usosBuggy, usosMoto);
    
    cout << costos[0][usosMoto][usosBuggy] << " ";
    //cout << cTemp << endl;
    int tempBuggy = usosBuggy;
    int tempMoto = usosMoto;
    int eleccion = 0;
    
    for(int i = 0; i < cantEtapas; i++) {
        eleccion = etapas[i][tempMoto][tempBuggy];
        if(eleccion == 2) {
            tempMoto--;
        } else if(eleccion == 3) {
            tempBuggy--;
        }
        cout << eleccion << " ";        
    }

    return 0;
}

int costoPDinamica(deque<int>& cBMX, deque<int>& cMoto, deque<int>& cBuggy, int act, int eTotal, int uBuggy, int uMoto, memoria& mem) {
    if(act == eTotal) {
        return 0;
        
    } else if(mem[act][uMoto][uBuggy] != -1) {
        return mem[act][uMoto][uBuggy];
        
    } else if(uBuggy == 0 && uMoto == 0) {
        mem[act][uMoto][uBuggy] = cBMX[act] + costoPDinamica(cBMX,cMoto,cBuggy,act+1,eTotal,0,0,mem);
        
    } else if(uBuggy == 0) {
        mem[act][uMoto][uBuggy] = min(
                cBMX[act] + costoPDinamica(cBMX,cMoto,cBuggy,act+1,eTotal,0,uMoto,mem),
                cMoto[act] + costoPDinamica(cBMX,cMoto,cBuggy,act+1,eTotal,0,uMoto-1,mem));
        
    } else if(uMoto == 0) {
        mem[act][uMoto][uBuggy] = min(
                cBMX[act] + costoPDinamica(cBMX,cMoto,cBuggy,act+1,eTotal,uBuggy,0,mem),
                cBuggy[act] + costoPDinamica(cBMX,cMoto,cBuggy,act+1,eTotal,uBuggy-1,0,mem));
        
    } else {
        mem[act][uMoto][uBuggy] = min(min(
                cBMX[act] + costoPDinamica(cBMX,cMoto,cBuggy,act+1,eTotal,uBuggy,uMoto,mem),
                cBuggy[act] + costoPDinamica(cBMX,cMoto,cBuggy,act+1,eTotal,uBuggy-1,uMoto,mem)),
                cMoto[act] + costoPDinamica(cBMX,cMoto,cBuggy,act+1,eTotal,uBuggy,uMoto-1,mem));
        
    }
    return mem[act][uMoto][uBuggy];
}

int costoRecursivo(deque<int>& cBMX, deque<int>& cMoto, deque<int>& cBuggy, int act, int eTotal, int uBuggy, int uMoto) {
    if(act == eTotal) {
        return 0;
    } else if(uBuggy == 0 && uMoto == 0) {
        return cBMX[act] + costoRecursivo(cBMX,cMoto,cBuggy,act+1,eTotal,0,0);
        
    } else if(uBuggy == 0) {
        return min(cBMX[act] + costoRecursivo(cBMX,cMoto,cBuggy,act+1,eTotal,0,uMoto),
                   cMoto[act] + costoRecursivo(cBMX,cMoto,cBuggy,act+1,eTotal,0,uMoto-1));
        
    } else if(uMoto == 0) {
        return min(cBMX[act] + costoRecursivo(cBMX,cMoto,cBuggy,act+1,eTotal,uBuggy,0),
                   cBuggy[act] + costoRecursivo(cBMX,cMoto,cBuggy,act+1,eTotal,uBuggy-1,0));
        
    } else {
        return min(min(
                cBMX[act] + costoRecursivo(cBMX,cMoto,cBuggy,act+1,eTotal,uBuggy,uMoto),
                cBuggy[act] + costoRecursivo(cBMX,cMoto,cBuggy,act+1,eTotal,uBuggy-1,uMoto)),
                cMoto[act] + costoRecursivo(cBMX,cMoto,cBuggy,act+1,eTotal,uBuggy,uMoto-1));
        
    }
}
