#include <iostream>
#include <math.h>
#include <deque>

using namespace std;

int main() {

	int exercice = 0;
	int seed = 0;
	int size = 0;
	int param0 = 0;
	int param1 = 0;
	int param2 = 0;
	int param3 = 0;
	int param4 = 0;
	int param5 = 0;
	int param6 = 0;
	string test = "";

	cin >> exercice >> seed >> size >> param0 >> param1 >> param2 >> param3 >> param4 >> param5 >> param6;

	srand(seed);

	//First exercice
	if(exercice == 1) {
		test = to_string(size) + " " + to_string(param0) + " " + to_string(param1) + "\n";

		for(int i = 0; i < size; i++) {
			test += to_string(rand() % 100) + " " + to_string(rand() % 100) + " " + to_string(rand() % 100) + "\n";
		}
	}
	//Second exercice
	if(exercice == 2) {
		
	}
	//Third exercice
	if(exercice == 3) {
		test = to_string(size) + " " + to_string(param0) + " " + to_string(rand() % 100) + "\n";

		deque<deque<bool>> pool;

		for(int i = 0; i < size; i++) {
			for(int j = 0; j < size; j++) {
				pool[i][j] = 0;
			}
		}

		for(int k = 0; k < param0; k++) {
			int i = (rand() % (size - 1)) + 1;
			int j = (rand() % (size - 1)) + 1;
			
			while(pool[i][j] == 1) {
				if(i > size) { j++; }
			}

			test += to_string(i) + " " + to_string(j) + " " + to_string(rand() % 100) + "\n";
			pool[i][j] = 1;
		}
	}

	cout << test;

	return 0;

}