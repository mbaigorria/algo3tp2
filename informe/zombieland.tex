\section{Problema 2: Zombieland II}

\subsection{Introducción}

Este ejercicio es una continuación del ejercicio 1 del TP 1. En una ciudad que no ha podido ser salvada, donde la cantidad de zombies fácilmente supera la cantidad de soldados en la ciudad, se encuentra un científico capaz de revertir los efectos del virus Z, responsable de la invasión zombie.

Para poder salvar al mundo y exterminar de una vez por todas a los zombies, una cuadrilla militar debe escoltar a este científico hasta un bunker militar ubicado en alguna esquina de la ciudad. Para ello, la cuadrilla debe moverse a pie por las diferentes calles de la ciudad.

Definamos a $z$ como la cantidad de zombies que hay en el tramo de una calle abarcado entre una esquina y la siguiente, y a $s$ como la cantidad de soldados vivos en cierta esquina. Al cruzar una calle, podrán ocurrir los siguientes desenlaces:

\begin{enumerate}
	\item Si $z \leq s$, los soldados matan a todos los zombies en la calle sin sufrir perdidas y pasan a la siguiente esquina.
	\item Si $z > s$, mueren $min(s, z-s)$ soldados. Es decir, puede suceder lo siguiente:
	\begin{enumerate}
		\item Si $z - s \geq s$, muere toda la cuadrilla y falla la misión.
		\item Si $z - s < s$, se matan a todos los zombies en la calle pero mueren algunos soldados. En total, se termina con $s - (z - s) = 2s - z$ soldados.
	\end{enumerate}
	
\end{enumerate}

Por lo tanto, la cantidad de soldados al cruzar una calle esta dada por la siguiente formula:

$soldados(s,z) = \begin{cases} s &\mbox{if } s \geq z \\
max(2s-z,0) & \mbox{if } s < z \end{cases}$

La ciudad tiene un trazado rectangular, con $n$ calles paralelas en forma horizontal y $m$ calles paralelas en forma vertical. Podemos modelar este problema con un grafo, donde cada esquina es representada por un nodo, y cada calle por una arista. Afortunadamente existe un enlace satelital que le permite a los soldados saber cuantos zombies se encuentran en los tramos de cada calle. 

El objetivo del problema es buscar el camino desde $I$ hasta $B$ que permite llevar con vida al científico y minimiza el numero de soldados muertos.
El algoritmo que resuelve el problema debe tener una complejidad temporal de \order{s \times n \times m}. A continuación se ilustra a modo de ejemplo una posible instancia inicial y su camino solución resaltado en color.

\begin{figure}[ht]
\centering
\begin{subfigure}[b]{0.4\textwidth}
	\includegraphics[scale=0.9]{Grafo_de_ejemplo_zombieland2_1.eps}
\end{subfigure}
\begin{subfigure}[b]{0.4\textwidth}
	\includegraphics[scale=0.9]{Grafo_de_ejemplo_zombieland2_2.eps}
\end{subfigure}
\caption{Ejemplo de una posible ciudad. El camino solución esta resaltado en la figura de la derecha en color.}
\end{figure}

Notar que no necesariamente existe un camino desde $I$ hasta $B$, dado que los soldados pueden ser insuficientes para pasar por alguna calle. A su vez tambien podemos ver que los algoritmos golosos no serán de mucha utilidad para resolver el problema, dado que no necesariamente se cruzara siempre la calle con el menor numero de zombies a partir de la esquina actual.

\newpage

\subsection{Algoritmo}

El problema a resolver es similar a los problemas clásicos de camino mínimo en grafos. Aunque cada arista en nuestro caso tiene como dato la cantidad de zombies en cierta calle, nosotros pensamos al costo de pasar de una esquina a otra como la cantidad de soldados que mueren al cruzar esa esquina. Esta cantidad, por como modelamos el problema, es siempre mayor o igual a 0.

A priori, podríamos pensar que esto cumple con los requerimientos necesarios para que Dijkstra nos garantice un camino mínimo. Sin embargo, nuestro problema tiene una complicación adicional. El costo de cruzar de una esquina a otra es dinámico y depende del recorrido que toman los soldados. Cruzar por una esquina no siempre cuesta lo mismo, dado que depende de la cantidad de soldados que han sobrevivido antes de cruzar la misma.

En la resolución del problema utilizaremos la siguiente notación:
\begin{itemize}
  \item $n$: Cantidad de calles horizontales.
  \item $m$ : Cantidad de calles verticales.
  \item $s$ : Cantidad de soldados con los que se cuentan en un principio.
  \item $C$ : Costo medido en cantidad de soldados perdidos para llegar al bunker.
  \item $L$ : Cantidad de soldados sobrevivientes que llegaron al bunker.
  \item $I_{h}$ : Número de calle horizontal sobre la que se encuentra la esquina de partida.
  \item $I_{v}$ : Número de calle vertical sobre la que se encuentra la esquina de partida.
   \item $B_{h}$ : Número de calle horizontal sobre la que se encuentra el bunker.
  \item $B_{v}$ : Número de calle vertical sobre la que se encuentra el bunker.
\end{itemize}

La idea inicial de la resolución propuesta a continuación fue adaptar el algoritmo de Dijkstra para que pueda comportarse bien ante costos dinámicos dependientes del estado anterior a un cruce. A su vez una pregunta relevante fue como se iba a representar el grafo, si con una lista o una matriz de adyacencia. Aquí fue donde comenzamos a buscar algunas particularidades de nuestro problema que nos pudiesen ayudar a encontrar una solución mas simple y a su vez que nos permitan justificar una complejidad de \order{s \times m \times n}. Las enunciaremos a medida que sean necesarias para la explicación.

Siguiendo a Dijkstra, nuestro algoritmo arranca en un nodo inicial $I$. En un principio sabemos que debido a que ninguna arista tiene costo negativo, no sera posible encontrar un camino mejor a este nodo.
Inicializamos y crearemos las siguientes estructuras/contenedores:
\begin{enumerate}
	\item Nodo: Struct que nos permite saber la cantidad de soldados máxima con la que se pudo llegar a un nodo en la iteracion actual del algoritmo. A su vez, nos permite saber desde que punto se llego.
	\item Coordenadas: Struct que nos permite guardar las coordenadas de un nodo en la cuadricula.
	\item callesHorizontales: Un vector$<vector<int>>$. Nos permite guardar el costo de cruzar una calle horizontal en tiempo lineal. Tiene $n \times (m-1)$ elementos.
	\item callesVerticales: Un $vector<vector<int>>$. Nos permite guardar el costo de cruzar una calle vertical en tiempo lineal. Tiene $(n-1) \times m$ elementos.
\end{enumerate}

	Todos los nodos serán inicializados sin un padre y con costo infinito, representado por el mismo numero $-1$. Al comenzar, se actualizara la cantidad de soldados del nodo $I$ con la cantidad de soldados que se tienen al comenzar la búsqueda.
	Utilizaremos una cola de prioridad FIFO, donde agregaremos los nodos que deben ser recorridos por el algoritmo. En un principio, agregamos solo el nodo $I$.
	
	Al igual que Dijkstra, se comienza recorriendo los nodos adyacentes. Consideremos en un primer lugar la siguiente proposición:
	
\begin{proposition}
Sea $G(V,E)$ un grafo no dirigido con una traza en cuadricula. Todo vértice $v \in V$ tiene a lo sumo $deg(v) \leq 4$
\end{proposition}

La demostración es bastante trivial, y en realidad viene directamente de la definición de trazado en cuadricula.
	
Esta proposición sera sumamente útil, dado que nos permitirá descartar algunas representaciones clásicas de grafos. Con una simple cuenta, podremos saber siempre cuales son los adyacentes a cualquier nodo. Cada vez que encontremos una mejora en el costo de un nodo actualizaremos su padre (para luego poder reconstruir un camino) y agregaremos las coordenadas del nodo a la cola. Iteraremos hasta que la cola este vaciá.

\newpage

Este procedimiento esta ilustrado en el siguiente pseudo-código. En un comienzo, se llama a la función Dijkstra con todos los datos en un estado consistente en memoria.

\begin{algorithmic}
	\Procedure{Dijkstra}{$G, initHorizontal, initVertical$}
	\State \textsc{Initializa-Single-Source($G,s$)}
	\State $nodeQueue = \emptyset$
	\State $nodeQueue \cup root$
	\While{$nodeQueue \neq \emptyset$}
		\State $u =$ S.front()
	\For{each vertex $v \in G.Adj[u]$}
		\State z = zombies en la arista que une u y v.
		\State \textsc{updatePath}($v, v.h, v.v, u.h, u.v, u.soldiers, z, nodeQueue$)
	\EndFor
	\State $nodeQueue - u$
	\EndWhile
	\EndProcedure
\end{algorithmic}

Para actualizar el camino, se verifica si efectivamente se debe relajar el nodo. La función getSoldiersLeft da la cantidad de soldados que sobrevivirían al cruzar la calle.

\begin{algorithmic}
	\Procedure{updatePath}{$destNode$, $dhAddr$, $dvAddr$, $hAddr$, $vAddr$, $s$, $z$ $nodoQueue$}
		\State soldiersLeft $\gets$ $getSoldiersLeft(s, z)$
		\If{soldiersLeft $>$ 0 $\wedge$ soldiersLeft $>$ destNode.soldiers}
			\State destNode.soldiers = soldiersLeft
			\State destNode.hAddr = hAddr
			\State destNode.vAddr = vAddr
			\State Cord $addCords(dhAddr, dvAddr)$
			\State nodoQueue.push(addCords)
		\EndIf
	\EndProcedure
\end{algorithmic}

Finalmente, para reconstruir el camino, lo hacemos desde el nodo final hacia atrás de forma recursiva. Esta función solo se llama si existe un camino y si el camino no comienza donde termina (caso donde el científico arranca en el bunker). En la cola solo guardamos las coordenadas del nodo en el trazado en cuadrilla.

\begin{algorithmic}
	\Procedure{getPath}{$initHorizontal$, $initVertical$, $map$}
		\If{map[initHorizontal][initVertical].hAddr = -1}
			\State \textbf{print} initHorizontal + 1, \textsc{\char13} \textsc{\char13}, initVertical + 1
		\Else
			\State $getPath$(map[initHorizontal][initVertical].hAddr, map[initHorizontal][initVertical].vAddr, map)
			\State \textbf{print} initHorizontal + 1, \textsc{\char13} \textsc{\char13}, initVertical + 1
		\EndIf	
	\EndProcedure
\end{algorithmic}

\newpage

\subsection{Correctitud}
La correctitud de nuestro algoritmo se prueba de manera análoga a como se prueba la de Dijkstra. La única diferencia esta en el procedimiento por el cual se relajan los ejes. Considerando que se relajan todos los nodos con todas las combinaciones posibles (esto se muestra en la sección de complejidad un poco mejor) dado que cualquier cambio se propaga a toda la red, los caminos serán mínimos.

\subsection{Complejidad}

La complejidad de nuestro algoritmo se puede descomponer en el siguiente conjunto de operaciones:
\begin{enumerate}
\item Inicializar la matriz de nodos: \order{n \times m}.
\item Inicializar los vectores de callesHorizontales y callesVerticales: \order{n \times m}.
\item Iterar nodos y actualizar sus vecinos.
\item Devolver la solución al problema: \order{n \times m}.
\end{enumerate}

Para definir la complejidad del punto 3, la siguiente proposición nos sera útil:

\begin{proposition}
Sea $G(V,E)$ un grafo ponderado no dirigido con una función de costo $w(v_1, v_2)$, con $v_1, v_2 \in V$ tal que $\exists$ un eje $(v_1,v_2) \in E$. Nuestro algoritmo de camino mínimo agrega cada nodo a la cola a lo sumo $s$ veces.
\end{proposition}

\begin{proof}
Un nodo se agrega a la cola cuando se encuentra un camino que hace que un mayor numero de soldados sobreviva. En el peor de los casos, en un principio un nodo es actualizado con 1 soldado. Esto puede seguir sucediendo hasta que el nodo es actualizado con $s$ soldados (sobreviven todos). Por lo tanto, cada nodo se puede encolar a lo sumo $s$ veces.
\end{proof}

Por lo tanto, la cola tendrá elementos adicionales a lo sumo $s$ por la cantidad de nodos $n \times m$. Por la Proposición 2.1, sabemos que cada nodo recorrerá a lo sumo 4 vértices. Verificar que un vértice no se puede \textit{relajar} es \order{1}. Por lo tanto la complejidad de este paso, que domina la complejidad del algoritmo es de \order{s \times n \times m}.