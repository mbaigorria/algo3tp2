\section{Problema 3: Refinando petroleo}

\subsection{Introducción}

  Dado un conjunto de pozos se busca una manera de coste óptimo de poder trasladar el petroleo de estos hacia alguna refinería. Cada una de éstas tiene un costo fijo, y sólo pueden ser construidas al lado de algún pozo, de manera que el pozo próximo a ellas ya estaría en condiciones de trasladar petroleo a la misma. Si bien no tenemos presente ninguna cota sobre la cantidad de refinerías a instalar, es posible que nos sea conveniente, financieramente hablando, conectar pozos entre si. Esto nos daría la ventaja de no tener que instalar una refinería para cada pozo, sino que por medio de dichas conexiones trasladar petroleo a alguna refinería ajena, aún pasando en su recorrido por otros pozos. Sin embargo, las única posibles redes de conexión implementables son las que podemos lograr por medio de tuberías, cada una de estas nos permite trasladar cualquier cantidad de petroleo de un extremo a otro, sin permitir ninguna bifurcación intermedia; además cada una de estas tiene un costo que varia según el caso, pudiendo incluso superar el de una refinería. Para saber que tuberías pueden ser implementadas, tenemos un conjunto prefijado de estas, entre las cuales podemos elegir cualquier subconjunto de ellas. Tanto este subconjunto de tuberías, como la decisión de en que pozos instalar una refinería o no, especificaran nuestra configuración como repuesta al problema.
  
   A modo de síntesis habrá que tener en cuenta la siguientes particularidades:

\begin{enumerate}
	\item Las refinerías sólo pueden construirse junto a alguno de los pozos disponibles. El costo por refinería $C$ es fijo. No hay límite en la cantidad de refinerías que pueden construirse.
	\item En todo pozo petrolero debe haber una refinería o un camino de tubos que permita llegar a algún otro pozo con una refinería asignada .
	\item Las tuberías no pueden bifurcarse entre dos pozos.
	\item Las tuberías son bidireccionales.
	\item Dada la geografía del lugar, no todo par de pozos puede conectarse por medio de alguna tubería (las conexiones factibles están predefinidas). 
	\item El costo de construcción de una tubería varia según el caso.
\end{enumerate}

\vspace{2mm}

Por ejemplo, consideremos la siguiente ilustración. Como podemos ver, no necesariamente todas los pozos estarán conectados. Pueden existir refinerías sin conexiones a otros pozos.

\begin{figure}[ht]
\centering
\begin{subfigure}[b]{0.45\textwidth}
	\includegraphics[scale=0.7]{Garfo_de_ejemplo_RefinandoPetroleo_1.eps}
\end{subfigure}
\begin{subfigure}[b]{0.45\textwidth}
	\includegraphics[scale=0.7]{Garfo_de_ejemplo_RefinandoPetroleo_2.eps}
\end{subfigure}
\caption{Ejemplo de una posible distribución de pozos. A la derecha se muestra la solución al problema, con las refinerías instaladas en verde.}
\end{figure}

El problema debe ser resuelto en \order{n^3}, donde $n$ es la cantidad de pozos.
\vfill

\vspace{2mm}
  
\subsection{Algoritmo}

  Para asimilar mejor el problema, hemos modelado la situación inicial con un grafo ponderado, en donde el conjunto de vértices es el conjunto de pozos, el de aristas es el conjunto de tuberías, y los pesos de cada una de estas últimas son los costos de las mismas. Según este modelo, son las aristas quienes nos sugieren la posibilidad de generar algún camino entre los vértices o pozos, y así facilitar el transado de su petroleo hacia algún otro vértice con una refinería ya asignada. Sin embargo, esto sólo nos sería de interés si alguna elección de dichas aristas nos resultara más barata que asignar una refinería para cada pozo contemplado en la cuestión.
  
   Con el fin de aliviar este análisis hemos simplificado desde un principio la resolución, eliminando aquellas aristas con un peso mayor a $C$ (costo de construcción de una refinería), la razón es bastante intuitiva. Suponiendo que estamos en proceso de construcción, en el que primero nos limitamos a seleccionar las aristas de interés, y postergamos hacia el final la tarea de elegir en que pozos instalar una refinería; si $G$ es el grafo que modela la entrada tal que $G$ es no trivial y $G'$ es el grafo hasta ahora elegido de forma que $V(G') = V(G) \land E(G') \subset E(G) \land G' \neq G$, dado un subgrafo conexo $S$ de $G'$ tal que $V(S) \subset V(G') \land V(S) \neq V(G')$, $\exists$ otro subgrafo conexo $F$ de $G'$, tal que $V(S) \cap V(F) = \emptyset$. Además asumiendo que $\exists H \subset E(G) \land H \neq \emptyset$ tal que $H$ sea el subconjunto de aristas que conectan a $S$ con $F$ en $G$, tendremos la opción de unir $S$ con $F$ por medio de alguna de ellas. Para ello sólo nos alcanzaría con usar una sóla arista, por ende defino $A$ como alguna arista perteneciente a $H$ tal que $A$ sea de costo mínimo en dicho grupo. Dado esto, en esta instacia puedo tener planificado facilitar el acceso de $S$ y $F$ a alguna refinería de las siguientes dos formas:
\begin{enumerate}
\item Ignorando cualquier arista de $H$, y en consecuencia afrontando la posibilidad de tener que construir dos refinerías, una para $S$ y otra para $F$.
\item Invirtiendo en $A$, y así garantizar la construcción de exactamente una sola refinería para $S \cup F$, pero afrontando el costo extra de $A$.
\end{enumerate}
 Ahora para este cometido según la opción elegida tendría que afrontar un aumento en el costo total de como mucho $2*C$ de la primera $vs$ un $C + costo(A)$ de la segunda. Si asumo que $costo(A) > C$, al elegir la segunda opción mi costo total se vería aumentado en $C + costo(A) > 2*C$ (costo máximo de la primera opción), por ende y en pos de minimizar el costo total, conviene bajo esa suposición ignorar a $A$ y optar por la primera opción. Es importante notar que a partir del caso en que valiera que $costo(A) < C$, no podríamos concluir cual opción es la óptima sólamente con las estimaciones anteriores, pues por ejemplo, podría existir otro subgrafos en $G'$ al cual $S$ y $F$ ya estuvieran conectados. Aunque en caso de que $costo(A) = C$ podríamos llegar a encontrarnos con que ambas opciones en algunas instancias nos mantenga en una configuración óptima, es prudente también ignorar estas aristas teniendo en cuenta que contribuirá a mejorar la complejidad espacial y temporal, sabiendo que por medio de la otra opción podremos llegar igual a un resultado óptima.
 
 Ya asumiendo que debo ignorar toda arista de un costo mayor a $C$, todavía tengo que decidir sobre que subconjunto de aristas restantes debo elegir. Para ello tuvimos en cuenta otro factor, la posible existencia de ciclos simples en nuestro grafo, estos nos sugerirían cierta redundancia, debido a que tendría mas de un camino para llegar a los pozos que recorrería ese ciclo. Llegamos a la conclusión que lo que esperábamos en nuestra configuración era un conjunto de grafos conexos y sin ciclos simples, también conocidos como árboles, de manera que nuestra abstracción gráfica del resultado sería un bosque generador. Pero además como pretendemos minimizar el costo total, no sólo queremos un bosque generador, sino un bosque generador mínimo. Para conseguirlo y a sabiendas de que podía cumplir la complejidad temporal exigida recurrimos al algoritmo de Kruskal. Este, en base a un grafo conexo y ponderado, construye -con una adecuada implementación- un bosque generador mínimo en una complejidad temporal de $O(n^2 \times log(n))$, donde $n$ es la cantidad total de pozos. Para ello es que desde un principio, ordenamos las tuberías de manera creciente, y creamos una estructura de datos sobre conjuntos disjuntos, que tenga una complejidad temporal de $O(log(n))$ para unir y localizar el conjunto al que pertenezca algún elemento ya insertado.
 Luego el costo total será la suma de los pesos de todas las aristas halladas en el grafo tras hacer Kruskal, mas el costo de una refinería multiplicado por la cantidad de árboles en el bosque generado.
 
 \vspace{2mm}
  \underline{A continuación explicaremos en que se basa y como funciona la estructura antes mencionada:}
  
 \vspace{1mm}
  Consiste en una estructura de bosque, en donde cada nodo identifica a un único elemento, y cada árbol es un único conjunto de estos. Dichos árboles, además se identifican con un único nodo raíz caracterizados por ser padres de si mismos, y que como todo nodo, además de resguardar la identidad de su padre actual, almacena la altura del árbol más grande que cuelga de él.
 
  Esta estructura está dotada de tres posibles operaciones principales:
 \begin{itemize}
  \item La operación \textsl{MAKE-SET}, que inserta un elemento nuevo a la estructura, realiza dicha inserción en tiempo constante, ya que simplemente significa crear un árbol de un sólo nodo (el elemento a insertar).
  
  
  \item La operación \textsl{FIND-SET}, que devuelve la identidad del conjunto al que pertenece un elemento ya insertado, logra su cometido accediendo al nodo en cuestión en tiempo constante y luego escalando desde allí hacia la raíz del árbol apelando únicamente a la identidad del padre resguardada en cada nodo de ese recorrido, por ende, su complejidad estará dictaminada por la longitud de esa sucesión. Veremos a continuación que debido al funcionamiento de  \textsl{UNION}, todo árbol en la estructura será un árbol balanceado, que cumpla que si $x$ es la cantidad de nodos en él, su altura estará acotada por $log(x) \leq log(n)$, y en consecuencia la complejidad de \textsl{FIND-SET} será de $O(log(n))$.
  
  
  \item La operación \textsl{UNION}, que dado dos elementos en la estructura, une los conjuntos a los que estos pertenecen, utiliza primero \textsl{FIND-SET}, identificando las raíces de los árboles de ambos elementos, para luego según éstas proseguir por alguno de los siguientes 3 casos:
 \begin{enumerate}
 \item Si para ambos elementos \textsl{FIND-SET} devuelve el mismo nodo, no hace nada (pues ambos elementos ya están en el mismo conjunto).
 \item Si \textsl{FIND-SET} devuelve nodos distinto y de distinta altura, le asigno al nodo de menor altura, al otro nodo como padre.
  \item Si \textsl{FIND-SET} devuelve nodos distinto y pero equivalentes en altura, le asigna a cualquiera de ellos, al otro nodo como padre. Y al que permaneció como raíz (su padre sigue siendo el mismo), le incrementa en 1 su altura.
 \end{enumerate}
 \end{itemize}
  Para mas información acerca de este tipo de estructuras: Cormen, T.,Leiserson, C.,Rivest,R.,Stein, C.,\textsl{"Introduction to Algorithms", Chapter 21: Data Structures for Disjoint Sets (pp. 561-585)}, The MIT Press, McGraw-Hill,2001.
  
\vspace{2mm}

En la resolución del problema utilizaremos la siguiente notación:
\begin{itemize}
  \item $n$: Cantidad de pozos.
  \item $m$ : Cantidad de tuberías posibles a contemplar.
  \item $C$ : Costo de construir una refinería.
  \item $K$ : Costo total de la solución.
  \item $r$ : Cantidad de refinerías a construir.
  \item $t$ : Cantidad de tuberías a construir.
  \item $T_{in}$ : Vector de tuberías posibles a contemplar.
  \item $T_{out}$ : Vector de tuberías resultante en nuestra solución. 
\end{itemize}

\vspace{2mm}

\begin{enumerate}

  \item Actualizo el $T_{in}$, ignorando todas las tuberías con un costo mayor o igual al de una refinería, y las ordeno de manera creciente con respecto a su costo.

  \item Creo $P$ como una estructura de datos sobre conjuntos disjuntos y la inicializo con $n$ arboles de un "nodo", de manera que cada uno guarde el número asociado a cada pozo, su padre y la altura del árbol generado a partir de él (el mismo y 0 respectivamente, en un principio).

  \item Aplico el siguiente algoritmo:
  	\vspace{2mm}
	\begin{algorithmic}
	\Procedure{Kruskal}{$T_{in}$, $P$, $K$, $T_{out}$}
		\vspace{2mm}
		\For{i = 0;\ i $<$ \textsl{tamaño($T_{in}$)} ;\ i{+}{+}}
			\vspace{1mm}
			\State  Tomo $H$ como la primera tubería del vector aún no elegida.
			\vspace{1mm}
			\State //Notese que este proceso podría pensarse e implementarse como una cola de prioridad, donde desencolo
			\State //las tuberias de menor a mayor, con respecto a su costo.
			\vspace{1mm}
	    		\State Defino $p1$ y $p2$ como los padres en la estructura P, para los pozos conectados por $H$.
			\vspace{1mm}
			\If{$p1 \neq p2$}
				\vspace{1mm}
		 		\State Agrego $H$ a $T_{out}$.
				\State Le incremento a $K$ el costo de $H$.
				\vspace{1mm}
				\If{La altura del árbol generado desde $p1$ es mayor o igual a la de $p2$}			\vspace{1mm}
					\State Redefino el padre de $p2$ como $p1$.
					\vspace{1mm}
					\If{La altura del árbol generado desde $p1$ es igual a la de $p2$}				\vspace{1mm}
						\State Aumento en 1 la altura de $p1$.
					\vspace{1mm}
					\EndIf
				\Else
					\State Redefino el padre de $p1$ como $p2$.
				\EndIf
			\EndIf
		\EndFor
	\EndProcedure
	\vspace{1mm}
	\end{algorithmic}

  \item Por último defino los valores de salida: 
  	\begin{itemize}
		\item $t$ con el tamaño($T_{out}$)
		\item $r$ con $n - t$
		\item Sumo a $K$ el valor de $C*r$
  	\end{itemize}
\end{enumerate}
Luego nuestra configuración óptima estará determinada por el conjunto de elementos de $T_{out}$ como tuberías elegidas, y el conjunto de raíces en $P$ como los pozos en donde se han de construir una refinería.

\vspace{2mm}

\subsection{Complejidad}

 \textsl{Nota: Los cálculos estimativos hechos en esta sección sobre complejidad aluden únicamente a complejidad temporal. Puede que para realizar dichas estimaciones utilicemos la notación de la sección anterior para referirnos a las variables que entran en juego.}

\vspace{4mm}

  En el primer paso el algoritmo actualiza $T_{in}$, quitandole toda tubería que tenga un costo mayor o igual al de una refinería. Este proceso de filtrado puede conseguirse fácilmente con una complejidad de $O(m)$, ya que simplemente se puede recorrer $T_{in}$ revisando cada tubería, comparando su costo, y en caso de no cumplir con el criterio explicado, quitar al elemento, ésto último se presume de costo de complejidad constante. Debido a que asumimos que para todo pozo $p1$ y $p2$ habrá como máximo sólo una tubería que los conecte y no habrá tuberías que conecten un pozo consigo mismo, ni que conecten pozos inexistentes, la cantidad de tuberías está acotada por la cantidad de aristas de un grafo completo con cantidad de vértices igual a la cantidad de pozos, en conclusión $\#Tuberias \leq n \times (n-1)/2$, por ende la complejidad de este proceso es de $O(m) \subseteq O(n \times(n-1)/2)$.
  Luego ordenar $T_{in}$ de manera creciente será $O(m \times log(n)) \subseteq O((n^2) \times log(n))$ usando algún algoritmo de $sorting$ como $HeapSort$ \footnote{Cormen, T.,Leiserson, C.,Rivest,R.,Stein, C.,\textsl{ "Introduction to Algorithms", Chapter 6: HeapSort (pp. 149-169)}, The MIT Press, McGraw-Hill,2001.}.
  
\vspace{4mm}

  En el segundo paso, creamos $P$ como una estructura de datos sobre conjuntos disjuntos, sobre una estructura de bosque, como se había explicado anteriormente, y la inicializamos creando un árbol trivial (de un sólo nodo) para cada pozo. Debido a que hacer \textsl{MAKE-SET} en $P$ nos es $O(1)$ \footnote{Cormen, T.,Leiserson, C.,Rivest,R.,Stein, C.,\textsl{ "Introduction to Algorithms", Chapter 21: Data Structures for Disjoint Sets (pp. 561-585)}, The MIT Press, McGraw-Hill,2001.}, y tenemos $n$ pozos, este proceso es $O(n)$.
  
\vspace{4mm}
  
  En el tercer paso, utilizamos el algoritmo de $Kruskal$ sobre la entrada $T_{in}$ y $P$, de la cual se infiere un grafo ponderado $G$, en donde \textsl{E(G) = \{ $e : e \in T_{in}$\}} y \textsl{V(G) = \{ $v : v \in P$\}}. Debido a que en nuestro algoritmo, $Kruskal$ itera como mucho $m$ veces, y tanto hacer  \textsl{UNION} como \textsl{FIND-SET} en $P$ tienen complejidad de $O(log(n))$ \footnote{Cormen, T.,Leiserson, C.,Rivest,R.,Stein, C.,\textsl{ "Introduction to Algorithms", Chapter 23: Minimum Spanning Trees (pp. 624-642)}, The MIT Press, McGraw-Hill,2001.}, hacer $Kruskal$ tiene una complejidad de $O(m*log(n))$ (3).
  
\vspace{4mm}
  
  En el cuarto y último paso, simplemente definimos las variables de salida. Para esto le solicitamos a $T_{out}$ su tamaño, la complejidad de ésta operación varia según el tipo de estructura del que se trate $T_{out}$, asumimos que ésto tendrá una complejidad de $O(1)$, si es que se ha guardado su tamaño en una variable actualizada tras cualquier operación en $T_{out}$, u $O(m)$ en el peor caso (en el que habría que contar la cantidad de elementos de $T_{out}$). El resto de los valores a asignar se obtienen en $O(1)$ (debido a que se trata de una resta y una multiplicación).