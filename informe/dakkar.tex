\section{Problema 1: Dakkar}

\subsection{Introduccion}

En una version similar al Rally Dakkar, un competidor debe decidir que vehículo utilizar para atravesar cada etapa en diferentes tipos de terrenos. El competidor tiene tres vehículos disponibles: Una bicicleta BMX, una motocross y un buggy arenero. Aunque el competidor tiene cronometrado el tiempo que tarda en cada etapa utilizando cada vehículo, por razones logísticas existen restricciones a la cantidad de veces que puede utilizar la motocross y el buggy arenero. Es decir, el competidor no necesariamente va a poder elegir el vehículo mas rápido en cada etapa. En caso de que ningún vehículo este disponible, siempre va a poder utilizar la BMX. El problema consiste en tener en cuenta dichas restricciones, y en base a eso elegir en cada etapa la mejor opción tal que el costo total sea el mínimo.

\vspace{2mm}

En la resolucion del problema utilizaremos la siguiente notacion:
\begin{itemize}
  \item n: \# etapas a recorrer.
  \item $k_m$: \# etapas máximas en las que puedo utilizar la moto.
  \item $k_b$: \# etapas máximas en las que puedo utilizar el buggy.
  \item $tiempos_{vehiculo}$: Vector de tiempos requeridos por un vehiculo $vehiculo \in \{bmx, moto, buggy\}$ para recorrer una $etapa$. Por ejemplo, $tiempos_{bmx}[0]$ es el tiempo que tarda una bmx en recorrer la primera etapa.

\end{itemize}

Los elementos $n$, $k_m$ y $k_b$ son números enteros, mientras que $tiempos_{bmx}$, $tiempos_{moto}$ y $tiempos_{buggy}$ son vectores de tipo entero con tamaño $n$. El problema debe ser resuelto en una complejidad temporal de \order{n*k_m*k_b}.

\vspace{2mm}

\textbf{Ejemplos del problema:}
\vspace{1 mm}

\textbf{Entrada:} $n = 3, k_m = 2, k_b = 1$

$tiempos_{bmx} = \{1,4,3\},\ tiempos_{moto} = \{5,2,3\},\  tiempos_{buggy} = \{4,1,3\} $

\textbf{Salida:} $costo = 5,\ camino = \{1,3,1\}$
\vspace{1 mm}

\textbf{Entrada:} $n = 4, k_m = 1, k_b = 2$

$tiempos_{bmx} = \{1,4,8,9\},\ tiempos_{moto} = \{2,3,4,2\},\  tiempos_{buggy} = \{3,5,5,5\} $

\textbf{Salida:} $costo = 14,\ camino = \{1,2,3,3\}$

%\vspace{2mm}
\subsection{Algoritmo}

Para resolver el problema, vamos a analizar en que consiste encontrar el costo mínimo. Esto lo podemos ver con la siguiente función recursiva. Comenzamos con 

$$
costoMinimo(e, k_m, k_b, n) =
\begin{cases}
0 & \text{si } e = n \\
tiempos_{bmx}[e] + costoMinimo(e, k_m, k_b, n) & \text{si } k_m = 0 \wedge k_b = 0\\

min\left(
	\begin{array}{c}
		tiempos_{bmx}[e] + costoMinimo(e+1, k_m, k_b, n) \\
		tiempos_{buggy}[e] + costoMinimo(e+1, k_m, k_b-1, n) \\
	\end{array}
\right) & \text{si } k_m = 0 \land k_b \neq 0\\

min\left(
	\begin{array}{c}
		tiempos_{bmx}[e] + costoMinimo(e+1, k_m, k_b, n) \\
		tiempos_{moto}[e] + costoMinimo(e+1, k_m-1, k_b, n) \\
	\end{array}
\right) & \text{si } k_b = 0 \land k_m \neq 0\\

min\left(
	\begin{array}{c}
		tiempos_{bmx}[e] + costoMinimo(e+1, k_m, k_b, n) \\
		tiempos_{moto}[e] + costoMinimo(e+1, k_m-1, k_b, n) \\
		tiempos_{buggy}[e] + costoMinimo(e+1, k_m, k_b-1, n) \\
	\end{array}
\right) & c/c\\
\end{cases}
$$

Esta función va a recorrer el espacio de soluciones completo, es decir, va a considerar todas las posibles combinaciones validas de caminos para después elegir el de menor costo total. Para saber el costo mínimo en n etapas, llamamos a la función desde la primera etapa $e=0$: $costoMinimo(0, k_m, k_b,n)$. Esta función se puede implementar de manera directa (de hecho una implementación se encuentra en nuestro código). Notese que esta función recursiva potencialmente puede realizar varias veces el mismo calculo. Por ejemplo, en el primer caso al evaluar la $costoMinimo(1,1,2)$, se llamaria recursivamente dos veces a $costoMinimo(3,0,2)$. Esto nos da el indicio de que estamos ante un problema con una \textbf{subestructura optima}. Debido a esto decidimos resolver este problema mediante un algoritmo de \textit{Programacion Dinamica}.

\subsubsection{Implementacion recursiva e iterativa}

Para la implementación con $Programacion\ Dinamica$ decidimos utilizar como memoria un arreglo de $n \times k_m \times k_b$ llamado $mem$, donde se van almacenando las soluciones parciales. Esto se debe a que para cada $n$ podemos tener $k_m \times k_b$ combinaciones posibles de motos y buggies disponibles. Esta matriz la inicializamos con todos sus valores en -1, para tener un indicador si un valor se encuentra o no en memoria. Esto tiene un costo de \order{n \times k_m \times k_b}. El pseudo-código de la implementación es el siguiente:

\vspace{2mm}

\begin{algorithmic}
	\Procedure{costoPDinamica}{$e$, $k_m$, $k_b$, $tiempos_{bmx}$, $tiempos_{moto}$, $tiempos_{buggy}$, $mem$}
		\If{$mem[e][k_m][k_b] \neq -1$}
			\Return $mem[e][k_m][k_b]$
		\Else
			\If{$e = n$}
				\State \Return $0$
			\ElsIf{$k_m = 0 \wedge k_b = 0$}
				\State $mem[e][k_m][k_b] = tiempos_{bmx}[e] + costoPDinamica(e+1, k_m, k_b, tiempos_{bmx}, tiempos_{moto}, tiempos_{buggy}, mem)$
			\ElsIf{$k_m = 0$}
				\State $mem[e][k_m][k_b] =$
				\State \hspace{12mm}$min(tiempos_{bmx}[e] + costoPDinamica(e+1,k_m,k_b,tiempos_{bmx},tiempos_{moto},tiempos_{buggy},mem),$
				\State \hspace{20mm}$tiempos_{buggy}[e] + costoPDinamica(e+1,k_m,k_b-1,tiempos_{bmx},tiempos_{moto},tiempos_{buggy},mem))$
			\ElsIf{$k_b = 0$}
				\State $mem[e][k_m][k_b] =$
				\State \hspace{12mm}$min(tiempos_{bmx}[e] + costoPDinamica(e+1,k_m,k_b,tiempos_{bmx},tiempos_{moto},tiempos_{buggy},mem),$
				\State \hspace{20mm}$tiempos_{moto}[e] + costoPDinamica(e+1,k_m-1,k_b,tiempos_{bmx},tiempos_{moto},tiempos_{buggy},mem))$
			\Else
				\State $mem[e][k_m][k_b] =$
				\State \hspace{12mm}$min(tiempos_{bmx}[e] + costoPDinamica(e+1,k_m,k_b,tiempos_{bmx},tiempos_{moto},tiempos_{buggy},mem),$
				\State \hspace{20mm}$tiempos_{moto}[e] + costoPDinamica(e+1,k_m-1,k_b,tiempos_{bmx},tiempos_{moto},tiempos_{buggy},mem),$
				\State \hspace{20mm}$tiempos_{buggy}[e] + costoPDinamica(e+1,k_m,k_b-1,tiempos_{bmx},tiempos_{moto},tiempos_{buggy},mem))$
			\EndIf 
		\EndIf
		
	\Return $mem[e][k_m][k_b]$
	\EndProcedure
\end{algorithmic}

\vspace{2mm}

Al resolver el problema recorriendo el árbol de decisión de forma bottom-up, si una subsolución se encuentra en memoria, la podemos buscar directamente allí sin tener que hacer el calculo completo. Caso contrario, se ocupa dicha posicion con el resultado de la llamada recursiva, que depende de las disponibilidad de $buggys$ y $motos$. Cada posición $i,j,k$ de memoria (es decir, $mem[i-1][j][k]$) contiene el costo mínimo entre la etapa $i$ y la ultima etapa, con $j$ usos restantes de $moto$ y $k$ usos restantes de $buggy$. Si tenemos el caso $n = k_m = k_b$ y la memoria esta vacía, la función se va a llamar recursivamente hasta llegar a $costoPDinamica(i,j,k)$ con $i = n$, $j \leq k_m$ y $k \leq k_b$. Posteriormente va a poder calcular $costoPDinamica(i-1,j',k')$ y así sucesivamente hasta llegar a $costoPDinamica(0,j'',k'')$ con $0 \leq j',j'' \leq j$ y $0 \leq k',k'' \leq k$, que fue desde donde llamamos y va a proceder con la próxima recursión hasta completar la memoria y tomar el mínimo. Esto nos da la pauta para poder hacer un planteo iterativo de la función, si llenamos la memoria desde $n$ hasta $1$ calculando el costo mínimo para todas las posibles combinaciones de $j$ y $k$ sabemos que a cada paso tenemos los cálculos necesarios en la memoria, con lo cual podemos plantear el siguiente pseudo-código:

\vspace{2mm}

\begin{algorithmic}
	\Procedure{costoIterativo}{$act$, $k_m$, $k_b$, $tiempos_{bmx}$, $costos_{moto}$, $costos_{buggy}$, $mem$}
		\For{$i = n-1;\ i > 0;\ i{-}{-}$}
			\For{$j = 0;\ j \leq k_m;\ j{+}{+}$}
				\For{$k = 0;\ k \leq k_b;\ k{+}{+}$}
					\If{$i = n-1$}
						\State tomo el mínimo entre los 3 vehículos en la ultima etapa según haya disponibilidad (es decir, reviso
						\State $j$ y $k$), y lo guardo en $mem[i][j][k]$
					\Else
						\State tomo el mínimo entre los 3 vehículos en la $i$-sima etapa según haya disponibilidad (es decir, reviso
						\State $j$ y $k$), lo sumo con $mem[i+1][j'][k']$ y lo guardo en $mem[i][j][k]$. $j'$ y $k'$ varían según el vehículo
						\State elegido en la $i$-esima etapa
					\EndIf
				\EndFor
			\EndFor
		\EndFor
		\State \Return $mem[0][k_m][k_b]$
	\EndProcedure
\end{algorithmic}

\vspace{2mm}

Una vez completados los ciclos, lo único necesario seria revisar el contenido de $mem[1][k_m][k_b]$ para obtener el costo mínimo.

\subsubsection{Almacenar camino}

Para poder construir el camino alcanza con tener otra memoria, también de tamaño $n \times k_m \times k_b$, a la cual nombramos $camino$, en la cual se van almacenando las elecciones tomadas en las distintas etapas. La version iterativa quedaría de la siguiente forma:

\begin{algorithmic}
	\Procedure{caminoIterativo}{$act$, $k_m$, $k_b$, $tiempos_{bmx}$, $costos_{moto}$, $costos_{buggy}$, $mem$, $camino$}
		\For{$i = n;\ i > 0;\ i{-}{-}$}
			\For{$j = 0;\ j \leq k_m;\ j{+}{+}$}
				\For{$k = 0;\ k \leq k_b;\ k{+}{+}$}
					\If{$i = n$}
						\State tomo el mínimo entre los 3 vehículos en la ultima etapa según haya disponibilidad (es decir, reviso
						\State $j$ y $k$), almaceno el vehículo en $camino[i][j][k]$ y el costo en $mem[i][j][k]$
					\Else
						\State tomo el mínimo entre los 3 vehículos en la $i$-esima etapa según haya disponibilidad (es decir, reviso
						\State $j$ y $k$), almaceno el vehículo en $camino[i][j][k$], sumo el costo con $mem[i+1][j'][k']$ y lo guardo en
						\State $mem[i][j][k]$. $j'$ y $k'$ varían según el vehículo elegido en la $i$-esima etapa
					\EndIf
				\EndFor
			\EndFor
		\EndFor
		\State \Return $mem[1][k_m][k_b]$
	\EndProcedure
\end{algorithmic}

\subsubsection{Construir camino mínimo}

Una vez construida la matriz $camino$ para poder reconstruir el camino alcanza con recorrerla de manera apropiada, para hacerlo hace falta inicializar dos contadores $i$, $j$ en $k_m$ y $k_b$, posicionarnos en $camino[1][i][j]$, almacenar el contenido de dicha posición en un vector, y según su contenido actualizar los contadores y proceder a la siguiente etapa hasta llegar al final. Esto se puede ver en el siguiente pseudo-código:

\begin{algorithmic}
	\Procedure{construirCamino}{$k_m$, $k_b$, $camino$}
		\State var: $i \gets k_m$
		\State var: $j \gets k_b$
		\State var: $res \gets$ \textsc{vector}($n$)
		\For{$h = 0;\ h < n;\ h{+}{+}$}
			\State $res[h] = cam[h][k_m][k_b]$
			\If{$res[h] = 2$}
				\State $i{-}{-}$
			\ElsIf{$res[h] = 3$}
				\State $j{-}{-}$
			\EndIf
		\EndFor
		\State \Return $res$
	\EndProcedure
\end{algorithmic}

\newpage

\subsection{Complejidad}

Para hacer el análisis de complejidad vamos a dar el siguiente pseudo-código, el cual describe el cuerpo de ejecución del programa:

\begin{algorithmic}
	\Procedure{resolver}{$k_m$, $k_b$, $tiempos_{bmx}$, $costos_{moto}$, $costos_{buggy}$}
		\State var: $mem \gets$ \textsc{Vector}($n \times k_m \times k_b$)
		\State var: $camino \gets$ \textsc{Vector}($n \times k_m \times k_b$)
		\State \textsc{inicializar}($mem$)
		\State var: $costoMinimo \gets$ \textsc{caminoIterativo}($0, k_m, k_b, tiempos_{bmx}, costos_{moto}, costos_{buggy}, mem, camino$)
		\State var: $caminoFinal \gets$ \textsc{construirCamino}($k_m, k_b, camino$)
		\State \Return $\langle costoMinimo, caminoFinal\rangle$
	\EndProcedure
\end{algorithmic}

\vspace{2mm}

La creación de las memorias $mem$ y $camino$ nos lleva $\mathcal{O}$($n \times k_m \times k_b$) cada una. La inicialización de la memoria consiste en recorrer las $n \times k_m \times k_b$ posiciones y llenarlas con $-1$. Esto también nos lleva $\mathcal{O}$($n \times k_m \times k_b$).

La función $caminoIterativo$ cuenta con tres bucles anidados, los cuales iteran $n$ veces, $k_m+1$ veces y $k_b+1$ veces respectivamente. Dentro del tercer bucle se realiza una cantidad acotada de comparaciones y asignaciones en tiempo constante, con lo cual la complejidad final de $caminoIterativo$ es $\mathcal{O}$($n \times k_m \times k_b$).

Una vez que completamos $caminoIterativo$ deberíamos tener en $camino$ las decisiones tomadas en cada etapa, con lo cual alcanza con llamar a $construirCamino$ para construir la solución final. Esta función cuenta con un único bucle que itera $n$ veces, en cada iteración se efectúa una cantidad acotada de comparaciones y asignaciones. La complejidad final de $construirCamino$ termina siendo $\mathcal{O}$($n$).

Teniendo en cuenta las complejidades mencionadas, la complejidad de $resolver$ seria la siguiente:

$$
4 \times \mathcal{O}(n \times k_m \times k_b) + \mathcal{O}(n) = \mathcal{O}(n \times k_m \times k_b)
$$

La complejidad final de nuestra solución propuesta es de $\mathcal{O}(n \times k_m \times k_b)$.

\subsection{Comentarios}
Aunque la solución anteriormente propuesta funciona bien, por momentos puede llegar a ser poco intuitiva. Consideremos la siguiente alternativa a la función de costo, con $e \leq n$

$$
costoMinimo(e, k_m, k_b) =
\begin{cases}
0 & \text{si } e \leq 0 \\
tiempos_{bmx}[e] + costoMinimo(e-1, k_m, k_b, n) & \text{si } k_m = 0 \wedge k_b = 0\\

min\left(
	\begin{array}{c}
		tiempos_{bmx}[e-1] + costoMinimo(e-1, k_m, k_b) \\
		tiempos_{buggy}[e-1] + costoMinimo(e-1, k_m, k_b-1) \\
	\end{array}
\right) & \text{si } k_m = 0 \land k_b \neq 0\\

min\left(
	\begin{array}{c}
		tiempos_{bmx}[e-1] + costoMinimo(e-1, k_m, k_b) \\
		tiempos_{moto}[e-1] + costoMinimo(e-1, k_m-1, k_b) \\
	\end{array}
\right) & \text{si } k_b = 0 \land k_m \neq 0\\

min\left(
	\begin{array}{c}
		tiempos_{bmx}[e-1] + costoMinimo(e-1, k_m, k_b) \\
		tiempos_{moto}[e-1] + costoMinimo(e-1, k_m-1, k_b) \\
		tiempos_{buggy}[e-1] + costoMinimo(e-1, k_m, k_b-1) \\
	\end{array}
\right) & c/c\\
\end{cases}
$$

Por lo tanto, si quiero saber el costo mínimo de n etapas, simplemente llamo la función de costoMinimo con $e = n$. Aquí se puede también de forma mas clara la \textbf{subestructura optima} del problema, dado que una carrera de $n$ etapas necesita todas las soluciones de problemas de $n-1$ etapas con diferentes vehículos disponibles.

Algo interesante que podemos notar es que si no tuviésemos que devolver la solución optima, podríamos reducir la complejidad espacial a \order{k_b \times k_m}. Esto se debe a que los problemas de $n$ etapas solo necesitan saber los óptimos de las etapas anteriores.